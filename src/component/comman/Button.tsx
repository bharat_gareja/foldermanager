import React, { ReactNode, MouseEvent } from 'react';

interface ButtonProps {
  onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
  children: ReactNode;
  type?: 'button' | 'submit' | 'reset' | undefined;
  title?: string;
}

const Button: React.FC<ButtonProps> = ({ onClick, children, type, title }) => {
  return (
    <button
      type={type}
      title={title}
      className='block text-white border-1 border-blue-400 hover:bg-blue-100 focus:ring-2 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-3 py-1 text-center mx-2'
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
