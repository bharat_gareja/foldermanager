import React from 'react';
import Foldermanager from './pages/Foldermanager';
import Header from './component/comman/Header';

const App: React.FC = () => {
  return (
    <>
      <Header />
      <div className='flex items-center justify-center'>
        <Foldermanager />
      </div>
    </>
  );
};

export default App;
