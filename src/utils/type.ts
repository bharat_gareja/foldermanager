export interface FolderType {
    id: number;
    name: string;
    isFolder: boolean;
    items: FolderType[];
}