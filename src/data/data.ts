import { FolderType } from "../utils/type";
const explorerData: FolderType = {
    id: 1,
    name: 'file manager',
    isFolder: true,
    items: [
        {
            id: 2,
            name: 'public',
            isFolder: true,
            items: [
                {
                    id: 4,
                    name: '.gitignore',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 5,
                    name: 'index.html',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 6,
                    name: 'package-lock.json',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 7,
                    name: 'manifest.json',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 8,
                    name: 'package.json',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 9,
                    name: 'README.md',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 10,
                    name: 'tsconfig.json',
                    isFolder: false,
                    items: [],
                },
            ],
        },
        {
            id: 3,
            name: 'src',
            isFolder: true,
            items: [
                {
                    id: 11,
                    name: 'App.css',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 12,
                    name: 'App.tsx',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 13,
                    name: 'index.css',
                    isFolder: false,
                    items: [],
                },
                {
                    id: 14,
                    name: 'components',
                    isFolder: true,
                    items: [
                        {
                            id: 15,
                            name: 'folder.jsx',
                            isFolder: false,
                            items: [],
                        },
                    ],
                },
                {
                    id: 16,
                    name: 'pages',
                    isFolder: true,
                    items: [
                        {
                            id: 17,
                            name: 'Home.jsx',
                            isFolder: false,
                            items: [],
                        },
                        {
                            id: 18,
                            name: 'About.jsx',
                            isFolder: false,
                            items: [],
                        },
                    ],
                },
            ],
        },
    ],
};

export default explorerData;
