// Header.js
import React, { useState } from 'react';

const Header = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <header className='bg-blue-500 py-4'>
      <div className='container mx-auto flex items-center justify-between'>
        <div className='text-white text-2xl font-bold capitalize'>folder manager</div>

        <div className='lg:hidden'>
          <button className='text-white text-2xl focus:outline-none' onClick={toggleMenu}>
            {isMenuOpen ? (
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path strokeLinecap='round' strokeLinejoin='round' strokeWidth={2} d='M6 18L18 6M6 6l12 12' />
              </svg>
            ) : (
              <svg
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path strokeLinecap='round' strokeLinejoin='round' strokeWidth={2} d='M4 6h16M4 12h16M4 18h16' />
              </svg>
            )}
          </button>
        </div>

        <nav className={`lg:flex ${isMenuOpen ? 'block' : 'hidden'}`}>
          <a href='/' className='block lg:inline-block text-white hover:text-gray-200 px-4 py-2'>
            Home
          </a>
          <a href='/' className='block lg:inline-block text-white hover:text-gray-200 px-4 py-2'>
            About
          </a>
        </nav>
      </div>
    </header>
  );
};

export default Header;
