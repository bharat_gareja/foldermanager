import React, { useState } from 'react';
import { FolderType } from '../utils/type';
import { AppDispatch, RootState } from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { updateFolderName, AddFolder, deleteFolder } from '../redux/slice/folderSlice';
import { AiOutlineDelete, AiFillEdit, AiFillFileAdd, AiFillFolderAdd, AiFillSave } from 'react-icons/ai';
import Button from '../component/comman/Button';

const Foldermanager: React.FC<any> = () => {
  const [isEditable, setIsEditable] = useState<boolean>(false);
  const [inputval, setInputval] = useState<string>('');
  const [editableval, setEditableVal] = useState<string>('');

  const dispatch = useDispatch<AppDispatch>();
  const folderStateData = useSelector((state: RootState) => state.folderState.explorerData);
  const [createFolder, setCreateFolder] = useState<{
    visibal: boolean;
    isFolder: boolean;
    folderId: number | null;
  }>({
    visibal: false,
    isFolder: false,
    folderId: null,
  });

  const handelCreateFolder = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, isFolder: any, folderId: number) => {
    e.stopPropagation();
    setCreateFolder({
      visibal: true,
      isFolder: isFolder,
      folderId: folderId,
    });
  };
  const [expFolders, setExpFolders] = useState<number[]>([]);

  const handelInputText = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputval(e.target.value);
  };

  const [editFolderId, setEditFolderId] = useState<number | null>(null);

  const handleEditButtonClick = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    folderData: FolderType,
    folderId: number
  ) => {
    e.stopPropagation();

    if (folderData.id === folderId) {
      setCreateFolder({
        visibal: true,
        isFolder: folderData.isFolder,
        folderId: folderId,
      });
      setEditableVal(folderData.name);
      setIsEditable(true);
      setEditFolderId(folderId);
    }
  };

  const handelEditText = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEditableVal(e.target.value);
  };

  const handelSubmit = (e: React.FormEvent<HTMLFormElement>, folderId: number) => {
    e.preventDefault();
    const initialdata: FolderType = {
      id: new Date().getTime(),
      name: inputval,
      isFolder: createFolder.isFolder,
      items: [],
    };
    dispatch(AddFolder({ folderId: folderId, initialdata }));
    setCreateFolder({ ...createFolder, visibal: false });
    setInputval('');
  };

  const handelEditSubmit = (e: React.FormEvent<HTMLFormElement>, folderId: number, folderName: string) => {
    e.preventDefault();
    dispatch(updateFolderName({ folderId: folderId, newName: editableval }));
    setIsEditable(false);
    setEditFolderId(null);
  };

  const handelDelete = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, folderId: number) => {
    e.stopPropagation();
    dispatch(deleteFolder(folderId));
  };
  const handelButtontoggle = (folderId: number) => {
    expFolders.includes(folderId)
      ? setExpFolders(expFolders.filter((id) => id !== folderId))
      : setExpFolders([...expFolders, folderId]);
  };

  const renderdata = (folderStateData: FolderType, parentId?: number) => {
    return (
      folderStateData.id > 0 && (
        <div className='pl-10 my-2 bg-violet-100 rounded-lg py-1'>
          {folderStateData.isFolder ? (
            <>
              <div
                className='bg-slate-300 flex justify-between cursor-pointer rounded-md shadow-md border-l-[3px] border-slate-700'
                onClick={() => handelButtontoggle(folderStateData.id)}
              >
                <h2 className='text-2xl px-3 py-1 font-semibold'>
                  <span className='text-xl'>📁</span>
                  {folderStateData.name}
                </h2>
                <div className='flex items-center'>
                  <Button onClick={(e) => handelCreateFolder(e, true, folderStateData.id)}>
                    <AiFillFolderAdd className='text-2xl text-amber-600' title='Add Folder' />
                  </Button>
                  <Button onClick={(e) => handelCreateFolder(e, false, folderStateData.id)}>
                    <AiFillFileAdd className='text-2xl text-blue-500' title='Add File' />
                  </Button>
                  <Button onClick={(e) => handelDelete(e, folderStateData.id)}>
                    <AiOutlineDelete className='text-2xl text-red-500' title='Delete Folder' />
                  </Button>
                </div>
              </div>

              <div className={`${expFolders.includes(folderStateData.id) ? 'block' : 'hidden'} pl-8`}>
                {createFolder.visibal && createFolder.folderId === folderStateData.id && (
                  <div className='flex my-2 items-center '>
                    <span className='text-1xl mx-2'>{createFolder.isFolder ? '📁' : '📄'}</span>
                    <form onSubmit={(e) => handelSubmit(e, folderStateData.id)} className='flex gap-2'>
                      <input
                        type='text'
                        onChange={(e) => handelInputText(e)}
                        value={inputval}
                        className='px-4 py-1 rounded-md border-3 border-gray-400 border '
                        required
                      />
                      <Button type='submit'>
                        {' '}
                        {createFolder.isFolder ? (
                          <AiFillFolderAdd className='text-2xl text-blue-500' title='Add Folder' />
                        ) : (
                          <AiFillFileAdd className='text-2xl text-blue-500' title='Add File' />
                        )}
                      </Button>
                    </form>
                  </div>
                )}
              </div>
              {expFolders.includes(folderStateData.id) &&
                folderStateData.items.map((item: FolderType) => (
                  <div key={item.id}>{renderdata(item, folderStateData.id)}</div>
                ))}
            </>
          ) : (
            <div className='flex justify-between'>
              <div>
                {!isEditable || folderStateData.id !== editFolderId ? (
                  <h4 className='font-semibold'>📄{folderStateData.name} </h4>
                ) : (
                  createFolder.visibal &&
                  createFolder.folderId === folderStateData.id && (
                    <div className='flex gap-3'>
                      <span className='text-1xl mx-2'> 📄 </span>
                      <form
                        className='flex gap-2'
                        onSubmit={(e) => handelEditSubmit(e, folderStateData.id, folderStateData.name)}
                      >
                        <input
                          type='text'
                          value={editableval}
                          onChange={(e) => handelEditText(e)}
                          className='px-4 py-1 rounded-md border-3 border-gray-400 border '
                          required
                        />
                        <Button type='submit' title='save'>
                          <AiFillSave className='text-blue-500 text-2xl' />
                        </Button>
                      </form>
                    </div>
                  )
                )}
              </div>
              <div className='flex gap-3 '>
                {!isEditable && (
                  <>
                    <Button onClick={(e) => handleEditButtonClick(e, folderStateData, folderStateData.id)}>
                      <AiFillEdit className='text-blue-500 text-2xl' title='Rename File' />
                    </Button>
                    <Button onClick={(e) => handelDelete(e, folderStateData.id)}>
                      <AiOutlineDelete className='text-2xl text-red-500' title='Delete File' />
                    </Button>
                  </>
                )}
              </div>
            </div>
          )}
        </div>
      )
    );
  };

  return <>{renderdata(folderStateData)}</>;
};

export default Foldermanager;
