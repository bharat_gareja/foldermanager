import {createSlice ,PayloadAction} from '@reduxjs/toolkit'
 import explorerData from '../../data/data'
 import { FolderType } from '../../utils/type';
 interface FolderState {
    explorerData: FolderType;
    isExpanded:boolean;
  }
  
 const initialState: FolderState = {
    explorerData: explorerData,
    isExpanded:false
  };
const folderSlice=createSlice({
    name:'folderState',
    initialState,
    reducers:{
        IsExpanded:(state,action: PayloadAction<boolean>)=>{state.isExpanded=action.payload},
        AddFolder: (state, action: PayloadAction<{ folderId: number; initialdata: FolderType }>) => {
            const { folderId, initialdata } = action.payload;
            const addDataFolder = (explorerData: FolderType, id: number) => {
                if (explorerData.id === id) {
                  // explorerData.items.unshift(initialdata);
                  explorerData.items = [initialdata, ...explorerData.items];
                } else if (explorerData.isFolder && explorerData.items) {
                  explorerData.items.map((item)=> addDataFolder(item, id))
                }
              };
              addDataFolder(state.explorerData, folderId);
          },

          deleteFolder: (state, action: PayloadAction<number>) => {
            const folderIdToDelete = action.payload;
            const deleteDataFolder = (explorerData: FolderType, idToDelete: number) => {
              if (explorerData.id === idToDelete) {
                explorerData.id = 0;
                explorerData.name = '';
                explorerData.isFolder = true;
                explorerData.items = [];
              } else if (explorerData.isFolder && explorerData.items) {
                explorerData.items = explorerData.items.filter((item) => item.id !== idToDelete);
                explorerData.items.map((item) => deleteDataFolder(item, idToDelete));
              }
            };
      
            deleteDataFolder(state.explorerData, folderIdToDelete);
          },

          updateFolderName: (state, action: PayloadAction<{ folderId: number; newName: string }>) => {
            const { folderId, newName } = action.payload;
        
            const updateFolderName = (explorerData: FolderType, id: number) => {

              if (explorerData.id === id) {
                explorerData.name = newName;
              } else if (explorerData.isFolder && explorerData.items) {
                explorerData.items.map((item) => updateFolderName(item, id));
              }
            };
        
            updateFolderName(state.explorerData, folderId);
          },
    }
})
export const{AddFolder,deleteFolder,updateFolderName,IsExpanded} = folderSlice.actions;
export default folderSlice.reducer;